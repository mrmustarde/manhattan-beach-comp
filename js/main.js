
$('.bg-chooser li').click(function(){
	var 	newClass = $(this).attr('class'),
			newBKG = $(this).find('img').attr('src').toString();

	$('.awesomeBG')
	.fadeOut('fast')
	.removeClass('full')
	.removeClass('pattern')
	.addClass(newClass)
	.css({
		'background-image' : 'url(' + newBKG + ')'
	}).hide().fadeIn('slow');
}).first().trigger('click');

$themeChooser = $('.theme-chooser li');

$('.theme-chooser li').click(function(){
	$('li').removeClass('current');
	var theIndex = $themeChooser.index(this) + 1;

	console.log(theIndex);

	$('.main li:nth-child(' + theIndex + ')').addClass('current');
});